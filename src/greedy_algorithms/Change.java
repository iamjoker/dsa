/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package greedy_algorithms;

import java.util.Scanner;

public class Change {
    private static int getChange(int m) {
        //write your code here
        
        return ((m-m%10)/10 + ((m%10)-((m%10)%5))/5 + m%5);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        System.out.println(getChange(m));

    }
}


