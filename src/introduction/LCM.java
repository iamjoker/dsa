/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package introduction;

import java.util.*;

public class LCM {
  private static long lcm(int a, int b) {
    int tmp;
    int c = a * b;
        while (a % b != 0) {
            tmp = a;
            a = b;
            b = tmp % a;
        }
        return c/b;
  }

  public static void main(String args[]) {
    Scanner scanner = new Scanner(System.in);
    int a = scanner.nextInt();
    int b = scanner.nextInt();

    System.out.println(lcm(a, b));
  }
}
