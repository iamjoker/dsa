/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package introduction;

import java.util.*;

public class FibonacciLastDigit {

    private static int getFibonacciLastDigit(int n) {
        ArrayList<Integer> Fib = new ArrayList<>();
    Fib.add(0);
    Fib.add(1);
    for(int i = 2; i <= n; i++){
        Fib.add(Fib.get(i-1) + Fib.get(i-2));
    }
    return Fib.get(n)%10;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int c = getFibonacciLastDigit(n);
        System.out.println(c);
    }
}
