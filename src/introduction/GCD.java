/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package introduction;

import java.util.*;

public class GCD {

    private static int gcd(int a, int b) {
        int tmp;
        while (a % b != 0) {
            tmp = a;
            a = b;
            b = tmp % a;
        }
        return b;
    }

    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();

        System.out.println(gcd(a, b));
    }
}
