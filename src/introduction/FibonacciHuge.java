/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package introduction;

import java.util.*;

public class FibonacciHuge {
    private static long getFibonacciHuge(long n, long m) {
        //write your code here

        ArrayList<Long> FibMod = new ArrayList();
        FibMod.add(0l);
        FibMod.add(1l);
        FibMod.add(1l);
        int k = 2;
        while (FibMod.get(k - 1) != 0 || FibMod.get(k) != 1) {
            k++;
            FibMod.add((FibMod.get(k - 1) + FibMod.get(k - 2)) % m);
        }
        long h = (n % (k - 1));
        return FibMod.get((int)h);
    }
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        long n = scanner.nextLong();
        long m = scanner.nextLong();
        System.out.println(getFibonacciHuge(n, m));
    }
}

