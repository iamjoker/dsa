/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package introduction;

import java.util.ArrayList;
import java.util.Scanner;

public class Fib {
  private static long calc_fib(int n) {
    ArrayList<Integer> Fib = new ArrayList<>();
    Fib.add(0);
    Fib.add(1);
    for(int i = 2; i <= n; i++){
        Fib.add(Fib.get(i-1) + Fib.get(i-2));
    }
    return Fib.get(n);
  }

  public static void main(String args[]) {
    Scanner in = new Scanner(System.in);
    int n = in.nextInt();
    System.out.println(calc_fib(n));
  }
}
